from guardian import GuardState
import time
import numpy as np
#######################
request = 'FINISHED'
nominal = 'FINISHED' 

###############################################
# reference values
###############################################
LINE_AMPLITUDE = 30000
DICT_TIME = {'last_timestamp' : 0}

###############################################
FREQARRAY = np.array([4001.3, 3501.3, 3001.3, 2501.3, 2001.3, 1501.3, 1001.3])
TIMEARRAY = np.array([8, 8, 5, 5, 2, 2, 2])
CUR_IND   = 0
###############################################

class INIT(GuardState):
    index = 0
    def main(self):
        log('Setting the initial freq')
        ezca['CAL-PCALX_PCALOSC1_OSC_FREQ'] = FREQARRAY[0]
        ezca['CAL-PCALX_PCALOSC1_OSC_SINGAIN'] = LINE_AMPLITUDE
        ezca['CAL-PCALX_PCALOSC1_OSC_COSGAIN'] = LINE_AMPLITUDE
        ezca['CAL-PCALX_PCALOSC1_OSC_TRAMP'] = 3.0
        DICT_TIME['last_timestamp']  =  ezca['FEC-124_TIME_DIAG']
        return 'IDLE'


class IDLE(GuardState):
    """Idle state for it to sit in most of the time.
    
    """
    index = 2
    def run(self):
        return True


class GETTING_READY(GuardState):
    index = 5
    request = False
    def main(self):
        DICT_TIME['last_timestamp'] = ezca['FEC-124_TIME_DIAG']
        CUR_FREQ = ezca['CAL-PCALX_PCALOSC1_OSC_FREQ']
        CUR_IND = np.where(FREQARRAY == round(CUR_FREQ,1))[0][0]
    def run(self):
        # if ezca['GRD-IFO_INTENT'] == 1: # Commenting out such that line frequency gets changed during commissioning time. JSK 2023-02-03
        if ezca['GRD-ISC_LOCK_STATE_N'] > 599:
            if  ezca['FEC-124_TIME_DIAG'] - DICT_TIME['last_timestamp'] >= TIMEARRAY[CUR_IND]*3601:
                return True
            else:
                time.sleep(1)
                return False
        else:
            time.sleep(1)
            DICT_TIME['last_timestamp'] = ezca['FEC-124_TIME_DIAG']
            return False

        
class CHECK_IFO_STATUS(GuardState):
    index = 15
    request = False
    def main(self):
        return
    def run(self):
        if ezca['GRD-ISC_LOCK_STATE_N'] <= 11:
            return True
        else:
            time.sleep(1)
            return False

        
class CHANGE_FREQ(GuardState):
    index = 20
    request = False
    def main(self):
        CUR_FREQ = ezca['CAL-PCALX_PCALOSC1_OSC_FREQ']
        if  (CUR_FREQ - 500) > 1000:
            NEW_FREQ = ezca['CAL-PCALX_PCALOSC1_OSC_FREQ'] - 500
            ezca['CAL-PCALX_PCALOSC1_OSC_FREQ'] = NEW_FREQ
            DICT_TIME['last_timestamp'] =  ezca['FEC-124_TIME_DIAG']
            return 'GETTING_READY'
        else:
            return 'INIT'

        
class FINISHED(GuardState):
    index = 25
    def main(self):
        return
    
    

######################
edges = [
    ('IDLE', 'GETTING_READY'),
    ('GETTING_READY','CHECK_IFO_STATUS'),
    ('CHECK_IFO_STATUS','CHANGE_FREQ'),
    ('CHANGE_FREQ','FINISHED'),
    ]
